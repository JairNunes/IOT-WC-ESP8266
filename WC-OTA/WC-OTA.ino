#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <FirebaseArduino.h>
#include <gpio.h>
#include <NTPClient.h>
#include <stdlib.h>


#define FIREBASE_HOST "raspbarry-78187.firebaseio.com"
#define FIREBASE_AUTH "CaCYkvChcdU7JSQ781kZdh7IfciovEhu8kV3WjB5"
#define WIFI_SSID "FC_CORP"
#define WIFI_PASSWORD "M@kech@nges"
#define PIN D1
#define TOILET_NUMBER 10

boolean state = false;
boolean enviado = false;
int count = 0;
WiFiUDP ntpUDP;

int16_t utc = -3; //UTC -3:00 Brazil
uint32_t currentMillis = 0;
uint32_t previousMillis = 0;


NTPClient timeClient(ntpUDP, "a.st1.ntp.br", -3 * 3600, 60000);


void setupPins() {
  pinMode(PIN, INPUT_PULLUP);
}

void setupWifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  timeClient.begin();

  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
}

void setupFirebase() {
  Firebase.begin(FIREBASE_HOST);
}


void setTime() {
  timeClient.update();
  Firebase.setInt(String("/toilets/toilet") + TOILET_NUMBER + "/timestamp", timeClient.getEpochTime());
}

void setToiletStatus(bool statusDoor) {
  Firebase.setBool(String("/toilets/toilet") + TOILET_NUMBER + "/available", statusDoor );
  setTime();
  if (Firebase.failed()) {
    Serial.print(" failed");
    Serial.println(Firebase.error());
    return;
  }
}

void toiletStatusChanged() {
  // lê o valor da porta do arduino
  if (digitalRead(PIN)) {
    count += 1;
    if (count == 1) {
      setToiletStatus(true);
      Serial.println("aberto");
    }
  } else {
    if (count) {
      setToiletStatus(false);
      Serial.println("fechado");
      count = 0;
    }
  }
  delay(200);

}

void setup() {
  Serial.begin(9600);
  Serial.begin(115200);
  Serial.println("Booting");

  setupWifi();
  setupFirebase();
  setupPins();

  timeClient.begin();
  timeClient.forceUpdate();

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]

  char toiletName[11] = "";

  String(String("banheiro") + TOILET_NUMBER).toCharArray(toiletName, 11);

  Serial.println(toiletName);

  ArduinoOTA.setHostname(toiletName);

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  ArduinoOTA.handle();
  toiletStatusChanged();
}
